#include <LGHeatpumpIR.h>
#include <IRsend.h>
#include <ir_LG.h>

LGHeatpumpIR::LGHeatpumpIR()
{
  static const char PROGMEM model[] PROGMEM = "lg";
  static const char PROGMEM info[]  PROGMEM = "{\"mdl\":\"lg\",\"dn\":\"LG\",\"mT\":18,\"xT\":31,\"fs\":3}";

  _model = model;
  _info = info;
}



void LGHeatpumpIR::send(IRSender& IR, uint8_t powerModeCmd, uint8_t operatingModeCmd, uint8_t fanSpeedCmd, uint8_t temperatureCmd, uint8_t swingVCmd, uint8_t swingHCmd)
{
  // Sensible defaults for the heat pump mode

  uint8_t powerMode     = LG_AIRCON1_MODE_ON;
  uint8_t operatingMode = LG_AIRCON1_MODE_HEAT;
  uint8_t fanSpeed      = LG_AIRCON1_FAN_AUTO;
  uint8_t temperature   = 23;
  uint8_t swingV        = LG_AIRCON1_VS_SWING;

  (void)swingHCmd;


  if (powerModeCmd == POWER_OFF)
  {
    powerMode = LG_AIRCON1_MODE_OFF;
    swingVCmd = VDIR_MUP;
  }

  switch (operatingModeCmd)
  {
    case MODE_AUTO:
      operatingMode = LG_AIRCON1_MODE_AUTO;
      break;
    case MODE_COOL:
      operatingMode = LG_AIRCON1_MODE_COOL;
      break;
    case MODE_DRY:
      operatingMode = LG_AIRCON1_MODE_DRY;
      break;
    case MODE_FAN:
      operatingMode = LG_AIRCON1_MODE_FAN;
      break;
    case MODE_HEAT:
      operatingMode = LG_AIRCON1_MODE_HEAT;
      break;  
  }

  switch (fanSpeedCmd)
  {
    case FAN_AUTO:
      fanSpeed = LG_AIRCON1_FAN_AUTO;
      break;
    case FAN_1:
      fanSpeed = LG_AIRCON1_FAN1;
      break;
    case FAN_2:
      fanSpeed = LG_AIRCON1_FAN2;
      break;
    case FAN_3:
      fanSpeed = LG_AIRCON1_FAN3;
      break;
  }

  if ( temperatureCmd > 15 && temperatureCmd < 31)
  {
    temperature = temperatureCmd;
  }

  switch (swingVCmd)
  {
    case VDIR_AUTO:
      swingV = LG_AIRCON1_VS_AUTO;
      break;
    case VDIR_SWING:
      swingV = LG_AIRCON1_VS_SWING;
      break;
    case VDIR_UP:
      swingV = LG_AIRCON1_VS_UP;
      break;
    case VDIR_MUP:
      swingV = LG_AIRCON1_VS_MUP;
      break;
    case VDIR_MIDDLE:
      swingV = LG_AIRCON1_VS_MIDDLE;
      break;
    case VDIR_MDOWN:
      swingV = LG_AIRCON1_VS_MDOWN;
      break;
    case VDIR_DOWN:
      swingV = LG_AIRCON1_VS_DOWN;
      break;
  }

  sendLG(IR, powerMode, operatingMode, fanSpeed, temperature, swingV, 0);
}




void LGHeatpumpIR::sendLG(IRSender& IR, uint8_t powerMode, uint8_t operatingMode, uint8_t fanSpeed, uint8_t temperature, uint8_t swingV, uint8_t swingH)
{
  uint8_t LGTemplate[] = { 0x8, 0x8, 0x0, 0x4, 0x7, 0x4};
  //                             0     1     2     3     4     5     6     7     8     9    10    11    12    13

  uint8_t checksum = 0x0;
  Serial.println(temperature,BIN);
  LGTemplate[4] = (temperature - 15);
  LGTemplate[3] = operatingMode;

  Serial.println("Temperature:");
  Serial.println(LGTemplate[4],BIN);
  Serial.println(LGTemplate[4],HEX);
  Serial.println("Mode:");
  Serial.println(LGTemplate[3],HEX);
  (void)swingH;

  // Set the operatingmode on the template message
  //LGTemplate[1] |= powerMode;
  //LGTemplate[2] |= operatingMode;

  // Set the temperature on the template message
  //LGTemplate[2] |= 31 - temperature;

  // Set the fan speed and vertical air direction on the template message
  //LGTemplate[8] |= fanSpeed | swingV;

  // Calculate the checksum
  //for (unsigned int i=1; i < (sizeof(LGTemplate)-1); i++) {
  //  checksum += LGTemplate[i];
  //}

  //LGTemplate[3] = checksum;

  // 38 kHz PWM frequency
 
  // Header
 // IR.mark(LG_AIRCON1_HDR_MARK);
 // IR.space(LG_AIRCON1_HDR_SPACE);
 uint16_t repeatHeaderMark = 0; 











  //We need to somehow send the checksum here...
/*   int nbits = 28;
 uint32_t elapsed = 0;
*/
int LGChecksum = 0; 
uint8_t checksumi = 0;
int intValue = 0;
int p; 
for (int j=0; j<sizeof(LGTemplate); j++) {
    uint8_t LGbyte = LGTemplate[j];
//    Serial.println(LGbyte,BIN);

    for (uint8_t k=0; k<8; k++) {
//      Serial.println(k,DEC);
      if ( (LGbyte & 0x01) == 0x01 ) {
        p = k; 
        if (k >= 4) { p = k - 4; }
//        Serial.println("Found a 1 at nibble position");
//        Serial.println(p,DEC);
//        Serial.println("Checksum");

        LGChecksum = LGChecksum + pow(2 , p);
//        Serial.println(LGChecksum,DEC);
      }
      LGbyte >>= 1;
    }
}

uint8_t checksumh = LGChecksum % 16;
//LGTemplate[3] = checksum;

//Serial.println("Checksum Total in HEX:");
//Serial.println(checksumh,HEX);

  IR.setFrequency(38);
  IR.mark(LG_AIRCON1_HDR_MARK);
  IR.space(LG_AIRCON1_HDR_SPACE);

    for (unsigned int m=0; m<sizeof(LGTemplate); m++) {
        Serial.println("Sending.. ");
        Serial.println(LGTemplate[m],HEX);
    //  IR.sendIRbyte(IR.bitReverse(LGTemplate[m]), LG_AIRCON1_BIT_MARK, LG_AIRCON1_ZERO_SPACE, LG_AIRCON1_ONE_SPACE);
        IR.sendIRnibble(1,IR.bitReverse(LGTemplate[m]),LG_AIRCON1_BIT_MARK, LG_AIRCON1_ZERO_SPACE, LG_AIRCON1_ONE_SPACE);
    }

//Serial.println(checksumh,BIN);
  IR.sendIRnibble(1,IR.bitReverse(checksumh), LG_AIRCON1_BIT_MARK, LG_AIRCON1_ZERO_SPACE, LG_AIRCON1_ONE_SPACE);
  
  IR.mark(LG_AIRCON1_BIT_MARK);
  IR.space(0);


/*
  
  for (uint64_t mask = 1ULL << (nbits - 1 ); mask; mask >>= 1) {
      if (data & mask) {
          IR.mark(LG_AIRCON1_BIT_MARK);
          IR.space(LG_AIRCON1_ONE_SPACE);
      } else {
        IR.mark(LG_AIRCON1_BIT_MARK);
        IR.space(LG_AIRCON1_ZERO_SPACE);
      }

}

IR.space(LG_AIRCON1_MIN_GAP);
*/   
/*
  for (unsigned int i=0; i<sizeof(LGTemplate); i++) 
  {

    //IR.sendIRbyte(LGTemplate[i], LG_AIRCON1_BIT_MARK, LG_AIRCON1_ZERO_SPACE, LG_AIRCON1_ONE_SPACE);
      IR.sendIRbyte(LGTemplate[i], LG_AIRCON1_BIT_MARK , LG_AIRCON1_ZERO_SPACE , LG_AIRCON1_ONE_SPACE);
  }

  // End mark
  IR.mark(LG_AIRCON1_BIT_MARK);
  IR.space(0);
  */
}

